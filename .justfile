# list all just commands
default:
    @just --list --unsorted

# initialize .env
init:
    #!/usr/bin/env bash
    [[ ! -f ./.env ]] && touch .env 

    for envline in VALHEIM_SERVER_NAME VALHEIM_SERVER_WORLD_NAME VALHEIM_SERVER_PASS \
                   CLOUDFLARE_API_KEY CLOUDFLARE_ZONE CLOUDFLARE_SUBDOMAIN CLOUDFLARE_PROXIED
    do
        read -p "${envline}: " envline_val
        # we need some way to append if sed can't find the expression
        sed -i "s/\(^${envline}=\).*/\1\"${envline_val}\"/g" .env
    done

# start server
start:
    @docker-compose up -d
    @lazydocker

# update server
update:
    @docker-compose up -d --force-recreate --pull always
    @lazydocker
